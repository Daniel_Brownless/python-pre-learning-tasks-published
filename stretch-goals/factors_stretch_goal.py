def factors(number):
    # ==============
    factor_list = []
    for i in range(2, number):
        if number % i == 0:
            factor_list.append(i)
    if factor_list == []:
        return "Thats a prime number!"
    else:
        return factor_list
    # ==============


while True:
    a = input("Enter a number: ")
    if a == "x":
        break
    else:
        n = int(a)
        print(factors(n))


# print(factors(15)) # Should print [3, 5] to the console
# print(factors(12)) # Should print [2, 3, 4, 6] to the console
# print(factors(13)) # Should print “[]” (an empty list) to the console
