def vowel_swapper(string):
    # ==============
    new_string = ""
    letters = ""
    vowel_dict = {
        "a": "4",
        "A": "/\\",
        "e": "3",
        "E": "3",
        "i": "!",
        "I": "!",
        "o": "ooo",
        "O": "000",
        "u": "|_|",
        "U": "|_|",
    }
    for letter in string:
        letters += letter
        if letters.count(letter) == 1 and letter in vowel_dict.keys():
            letter2 = vowel_dict.get(letter)
            new_string += letter2
        else:
            new_string += letter
    return new_string
    # ==============


print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
